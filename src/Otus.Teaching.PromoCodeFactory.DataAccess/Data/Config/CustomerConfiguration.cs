﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data.Config
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.ToTable("Customers");
            builder.HasKey(x => x.Id);
            
            builder.Property(c => c.FirstName)
                .IsRequired()
                .HasMaxLength(150);
            
            builder.Property(c => c.LastName).
                IsRequired().
                HasMaxLength(150);
            
            builder.Property(c => c.Email)
                .IsRequired().HasMaxLength(150);

            //builder.HasMany(c => c.PromoCodes)
            //    .WithOne(c => c.Customer)
            //    .HasForeignKey(c => c.CustomerId);
        }
    }
}
