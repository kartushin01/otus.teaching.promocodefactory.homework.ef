﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data.Config
{
    public class PromocodeCustomerConfiguratin : IEntityTypeConfiguration<PromocodeCustomer>
    {
        public void Configure(EntityTypeBuilder<PromocodeCustomer> builder)
        {
            builder.ToTable("PromocodesCustomers");

            builder.Ignore(x => x.Id);

            builder.HasKey(pc => new
            {
                pc.CustomerId,
                pc.PromocodeId
            });

            builder.HasOne(pc => pc.Customer)
                .WithMany(p => p.PromocodesCustomers)
                .HasForeignKey(x => x.CustomerId);

            builder.HasOne(pc => pc.PromoCode)
                .WithMany(p => p.PromocodesCustomers)
                .HasForeignKey(x => x.PromocodeId);


        }
    
    }
}
