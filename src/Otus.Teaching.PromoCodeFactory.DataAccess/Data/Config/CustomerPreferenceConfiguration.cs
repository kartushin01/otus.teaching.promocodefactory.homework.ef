﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data.Config
{
    class CustomerPreferenceConfiguration : IEntityTypeConfiguration<CustomerPreference>
    {
        public void Configure(EntityTypeBuilder<CustomerPreference> builder)
        {
            builder.ToTable("CustomersPreferences");
            
            builder.Ignore(x => x.Id);
            
            builder.HasKey(cp => new
            {
                cp.CustomerId, cp.PreferenceId
            });

            builder.HasOne(cp => cp.Customer)
                .WithMany(p => p.CustomerPreferences)
                .HasForeignKey(x=>x.CustomerId);

            builder.HasOne(cp => cp.Preference)
                .WithMany(p => p.CustomerPreferences)
                .HasForeignKey(x => x.PreferenceId);


        }
    }
}
