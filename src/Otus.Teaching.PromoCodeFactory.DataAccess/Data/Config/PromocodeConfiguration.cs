﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data.Config
{
    class PromocodeConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.ToTable("PromoCodes");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.ServiceInfo).HasMaxLength(150);

            builder.Property(x => x.PartnerName).HasMaxLength(150);

            builder.Property(x => x.Code).HasMaxLength(8);

            builder.HasOne(c => c.Preference)
                .WithOne(p => p.PromoCode)
                .HasForeignKey<PromoCode>(p => p.PreferenceId);

        }
    }
}
