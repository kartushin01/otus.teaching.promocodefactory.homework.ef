﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data.Config
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.ToTable("Employees");
           
            builder.HasKey(x => x.Id);
            
            builder.Property(c => c.FirstName)
                .IsRequired()
                .HasMaxLength(150);
           
            builder.Property(c => c.LastName)
                .IsRequired()
                .HasMaxLength(150);
            
            builder.Property(c => c.Email)
                .IsRequired()
                .HasMaxLength(150);

            builder.HasOne(r => r.Role)
                .WithOne(e => e.Employee)
                .HasForeignKey<Role>(e => e.EmployeeId);


        }
    }
}
