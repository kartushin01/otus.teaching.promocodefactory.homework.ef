﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class PromoContextSeed
    {

        public static async Task SeedAsync(PromoContext context)
        {
            
                if (!context.Customers.Any())
                {
                    await context.Customers.AddRangeAsync(FakeDataFactory.Customers);
                    await context.SaveChangesAsync();
                }
                if (!context.Employees.Any())
                {
                    await context.Employees.AddRangeAsync(FakeDataFactory.Employees);
                    await context.SaveChangesAsync();
                }
                if (!context.Roles.Any())
                {
                    await context.Roles.AddRangeAsync(FakeDataFactory.Roles);
                    await context.SaveChangesAsync();
                }
                if (!context.Preferences.Any())
                {
                    await context.Preferences.AddRangeAsync(FakeDataFactory.Preferences);
                    await context.SaveChangesAsync();
                }
        }
    }
}
