﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore.Internal;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerSpicificRepository : EfRepository<Customer>, ICustomerSpecificRepository
    {

        public CustomerSpicificRepository(PromoContext context):base(context)
        {

        }

        public async Task<List<Customer>> GetCustomersWithPtreferneces(string preferenceName)
        {
            return await _dbContext.Customers
                .Include(x => x.CustomerPreferences)
                .ThenInclude(p => p.Preference).Where(c => c.CustomerPreferences.Any(d => d.Preference.Name == preferenceName)).ToListAsync();
        }
    }
}
