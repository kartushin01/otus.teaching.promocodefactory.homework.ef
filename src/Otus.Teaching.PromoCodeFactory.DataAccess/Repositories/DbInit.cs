﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class DbInit : IDbInit
    {
        public async Task DataSeed(PromoContext context)
        {
            var migrations = context.Database.GetMigrations();

            if (!migrations.Any())
            {
                await context.Database.EnsureDeletedAsync();
                await context.Database.EnsureCreatedAsync();
            }

            var pendingMigrations = context.Database.GetPendingMigrations();

            if (pendingMigrations.Any())
            {
                await context.Database.MigrateAsync();
            }

            await PromoContextSeed.SeedAsync(context);
        }

    }
}
