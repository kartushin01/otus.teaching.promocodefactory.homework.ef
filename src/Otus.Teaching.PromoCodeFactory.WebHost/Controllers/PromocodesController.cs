﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {

        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public PromocodesController(
            IRepository<PromoCode> promocodeRepository,
            IRepository<Customer> customerRepository,
            IRepository<Employee> employeeRepository, 
            IRepository<Preference> preferenceRepository
           )
        {
            _promocodeRepository = promocodeRepository;
            _customerRepository = customerRepository;
            _employeeRepository = employeeRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [ActionName("GetPromocodesAsync")]
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocedes = await _promocodeRepository.GetAllAsync(x=>x.Include(m=>m.PartnerManager));

            var model = promocedes.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                ServiceInfo = x.ServiceInfo,
                BeginDate = x.BeginDate.ToString("d"),
                EndDate = x.EndDate.ToString("d"),
                PartnerName = x.PartnerName

            }).ToList();

            return model;
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            
            //Получаем партнёрского менеджера, не по нимени, а по email
            var partner = await _employeeRepository.GetFirstWhere(x => x.Email == request.PartnerName);

            //Получаем предпочтение по имени
            var preference = await _preferenceRepository.GetFirstWhere(x => x.Name == request.Preference);

            if (partner == null || preference == null)
            {
                BadRequest();
            }

            //  Получаем клиентов с этим предпочтением:
           
            var customers = await _customerRepository.GetWhere(d=>d.CustomerPreferences.Any(n=>n.Preference.Name == request.Preference),x => x.Include(cp => cp.CustomerPreferences).ThenInclude(p => p.Preference));
           
            PromoCode promocode = PromocodeStaticMapper.MapGromModel(request, partner, preference, customers as List<Customer>);

            await _promocodeRepository.CreateAsync(promocode);

            return CreatedAtAction(nameof(GetPromocodesAsync), new { }, null);


        }
    }
}