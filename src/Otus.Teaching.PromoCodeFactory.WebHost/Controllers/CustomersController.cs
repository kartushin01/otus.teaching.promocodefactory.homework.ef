﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {

        private readonly IRepository<Customer> _customerRepository;
   
        private readonly IRepository<Preference> _preferencesRepository;

        private readonly IDbInit _dbInit;
        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferencesRepository, IDbInit dbInit)
        {
            _customerRepository = customerRepository;
            _preferencesRepository = preferencesRepository;
            _dbInit = dbInit;
        }


        /// <summary>
        /// Получение всех клиентов
        /// </summary>
        /// <returns>List CustomerShortResponse</returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {

     
            var customers = await _customerRepository.GetAllAsync(x=>x.Include(p=>p.CustomerPreferences));

            return customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email
            }).ToList();

        }
        /// <summary>
        /// Получение одного клиентв
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ActionName("GetCustomerAsync")]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {

            Customer customer = await _customerRepository
                .GetByIdAsync(id, x => x.Include(x => x.CustomerPreferences)
                .ThenInclude(c => c.Preference)
                .Include(p => p.PromocodesCustomers)
                .ThenInclude(c => c.PromoCode));

            if (customer == null)
            {
                return NotFound();
            }


            CustomerResponse model = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = customer.CustomerPreferences.Select(p => new PreferenceResponse()
                {
                    Id = p.PreferenceId,
                    Name = p.Preference.Name

                }).ToList()
            };

            if (customer.PromocodesCustomers.Any())
            {

                model.PromoCodes = customer.PromocodesCustomers.Select(x => new PromoCodeShortResponse()
                {
                    Id = x.PromocodeId,
                    Code = x.PromoCode.Code,
                    PartnerName = x.PromoCode.PartnerName,
                    BeginDate = x.PromoCode.BeginDate.ToString("d"),
                    EndDate = x.PromoCode.EndDate.ToString("d"),
                    ServiceInfo = x.PromoCode.ServiceInfo

                }).ToList();
            };

            return model;

        }
        /// <summary>
        /// Создание нового клиента со списком его предпочтений
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {

            var preferences = await _preferencesRepository.GetWhere(c => request.PreferenceIds.Contains(c.Id)) as List<Preference>;

            Customer customer = CustomerStaticMapper.MapFromModel(request, preferences);

            await _customerRepository.CreateAsync(customer);

            return CreatedAtAction(nameof(GetCustomerAsync), new { id = customer.Id }, null);

        }
        /// <summary>
        /// Изменение клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {

            Customer customerFromBd = await _customerRepository
                .GetByIdAsync(id, x => x.Include(x => x.CustomerPreferences)
                .ThenInclude(c => c.Preference)
                .Include(p => p.PromocodesCustomers)
                .ThenInclude(c => c.PromoCode));

            if (customerFromBd == null)
            {
                return BadRequest();
            }

            var preferences = await _preferencesRepository.GetWhere(c => request.PreferenceIds.Contains(c.Id)) as List<Preference>;

            var customerToUpdate = CustomerStaticMapper.MapFromModel(request, preferences, customerFromBd);

            await _customerRepository.UpdateAsync(customerToUpdate);

            return NoContent();

        }
        /// <summary>
        /// Удаление клиента с его промо кодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {

            Customer customer = await _customerRepository
                                        .GetByIdAsync(id, x => x
                                            .Include(cp => cp.CustomerPreferences)
                                            .ThenInclude(p => p.Preference)
                                            .Include(pc => pc.PromocodesCustomers)
                                            .ThenInclude(p => p.PromoCode));

            if (customer == null)
            {
                return NotFound();
            }

            await _customerRepository.DeleteAsync(customer);

            return NoContent();
        }
    }
}