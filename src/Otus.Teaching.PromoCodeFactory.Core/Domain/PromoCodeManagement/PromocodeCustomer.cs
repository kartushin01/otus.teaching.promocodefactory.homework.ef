﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromocodeCustomer : BaseEntity
    {
        public Guid PromocodeId { get; set; }
        public PromoCode PromoCode { get; set; }

        public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}
