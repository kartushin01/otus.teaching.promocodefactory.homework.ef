﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomerSpecificRepository : IRepository<Customer>
    {

       Task<List<Customer>> GetCustomersWithPtreferneces(string preferenceName);

    }
}
